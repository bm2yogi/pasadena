# README #

### What is this repository for? ###

* I use the code in this repo to evaluate .NET candidates as part of my company's interview process.
* Version 1.0.0

### How do I get set up? ###

You'll need a copy of Visual Studio 2013. The solution should be ready to run locally once you've run nuget to update the dependent packages.

There are no tests written for any of this code. That's part of the process.

### Who do I talk to? ###

* This repo is maintained by Michael Ibarra. You can email me at bm2yogi@gmail.com