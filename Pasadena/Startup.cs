﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pasadena.Startup))]
namespace Pasadena
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
